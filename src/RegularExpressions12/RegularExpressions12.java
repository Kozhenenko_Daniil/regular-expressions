package RegularExpressions12;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressions12 {
    public static void main(String[] args) {
        int counter=0;
        String IPv6 = "1:1:1:1:1:1:1:1";

        Pattern pattern = Pattern.compile("((^|:)([0-9a-fA-F]{0,4})){1,8}$");
        Matcher matcher = pattern.matcher(IPv6);

        while (matcher.find()) {
            counter++;
            System.out.println("Match found " +
                    IPv6.substring(matcher.start(), matcher.end()) +
                    "Starting at index " + matcher.start() +
                    " and ending at index " + matcher.end());

            System.out.println("Matches found: " + counter);
        }
    }

}
