package RegularExpressions3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressions3 {
    public static void main(String[] args) {

        Pattern pattern = Pattern.compile("Java (?=[78])");
        Matcher matcher = pattern.matcher("Java 7  Java 8");
        while (matcher.find()){
            System.out.println(matcher.group());
        }
    }
}
