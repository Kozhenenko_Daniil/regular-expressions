package RegularExpressions2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressions2 {

    public static void main(String[] args) {

        Pattern pattern = Pattern.compile("[\\S]");

        Matcher matcher1 = pattern.matcher("a b c d e f g h");
        System.out.println(matcher1.find());

        Matcher matcher2 = pattern.matcher("f g h a b c");
        System.out.println(matcher2.find());

        Matcher matcher3 = pattern.matcher("2");
        System.out.println(matcher3.find());
    }
}